package consumer

func (c *Consumer) bind() error {
	err := c.ch.DeclareQueue(c.opts.MustBind, c.opts.MustVerify, c.opts.QueueOpts)
	if err != nil {
		return err
	}
	// declares(declare or verify) exchange then binds  to a queue
	_, err = c.ch.DeclareExchange(c.opts.ExchangeBindingOpts, true, c.opts.QueueOpts)
	if err != nil {
		return err
	}
	return nil
}
