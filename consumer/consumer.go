package consumer

import (
	"errors"
	"log"
	"sync"

	"gitlab.com/go-helpers/wabbit"
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

var (
	ErrNoQueueSpecified = errors.New("no queue specification specified")
)

/*
	Consumer for queue
*/
type Consumer struct {
	conn         wabbitiface.WrappedConnection
	ch           wabbitiface.WrappedChannel
	notifierHook wabbitiface.Notify
	handler      Handler

	m                   *sync.Mutex
	opts                *opts.Options
	disconnectedErrChan chan error
	reconnectecChan     chan bool
	wg                  *sync.WaitGroup
}

func WithNotifierHook(fn wabbitiface.Notify) func(*Consumer) {
	return func(c *Consumer) {
		c.notifierHook = fn
	}
}

func New(conn *wabbit.Connection, options ...func(*opts.Options)) (*Consumer, error) {
	consumer := &Consumer{
		m:                   &sync.Mutex{},
		opts:                &opts.Options{},
		disconnectedErrChan: make(chan error),
		reconnectecChan:     make(chan bool),
		wg:                  &sync.WaitGroup{},
	}
	for _, fn := range options {
		fn(consumer.opts)
	}
	if len(consumer.opts.QueueOpts) == 0 {
		return nil, ErrNoQueueSpecified
	}
	consumer.conn = conn
	consumer.conn.SetNotifierFunc(func(err error, itf ...interface{}) {
		consumer.disconnectedErrChan <- err
	})

	if err := consumer.setupChannel(); err != nil {
		return nil, err
	}

	return consumer, nil
}

func (c *Consumer) Start(handler Handler) error {
	deliveries, err := c.Consume()
	if err != nil {
		return err
	}

	go c.handleConsumeDeliveries(deliveries, handler)

	return nil
}

func (c *Consumer) reconnect() error {
	if err := c.conn.Connect(); err != nil {
		return err
	}
	return c.setupChannel()
}

func (c *Consumer) setupChannel() error {
	// create channel from connection
	channel, err := c.conn.Channel()
	if err != nil {
		return nil
	}
	c.ch = channel

	if c.opts.UseGlobalPrefetch() {

	}
	log.Print("wabbit:consumer channel established")

	if err := c.bind(); err != nil {
		return err
	}

	return nil
}
