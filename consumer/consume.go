package consumer

import (
	"log"
	"time"

	"github.com/gofrs/uuid"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit"
	"gitlab.com/go-helpers/wabbit/opts"
)

func (c *Consumer) Consume() (map[*opts.QueueInfo]<-chan amqp.Delivery, error) {
	m := make(map[*opts.QueueInfo]<-chan amqp.Delivery)
	for k, _ := range c.opts.QueueOpts {
		consumerID := uuid.Must(uuid.NewV4()).String()
		k.ConsumerID = consumerID
		deliveries, err := c.ch.Consume(k.Name, consumerID, false, false, false, false, nil)
		if err != nil {
			return nil, err
		}
		m[k] = deliveries
	}
	return m, nil
}

func (c *Consumer) consumeDelivery(queueInfo *opts.QueueInfo, delivery <-chan amqp.Delivery, handler Handler) {
	for d := range delivery {
		handler.ProcessMessage(queueInfo, wabbit.NewWrappedDelivery(d))
	}
}

func (c *Consumer) handleConsumeDeliveries(deliveries map[*opts.QueueInfo]<-chan amqp.Delivery, handler Handler) {
	for {
		for queueInfo, delivery := range deliveries {
			go c.consumeDelivery(queueInfo, delivery, handler)
		}
		if err := <-c.disconnectedErrChan; err != nil {
			log.Println("wabbit:consumer disconnected", err.Error())
			conErr := c.reconnect()
			for conErr != nil {
				conErr = c.reconnect()
				time.Sleep(50 * time.Millisecond)
			}
			log.Println("wabbit:consumer reconnected")
			deli, err := c.Consume()
			if err != nil {
				log.Println("wabbit:consumer failed to consume, continuing")
				continue
			}
			deliveries = deli
		}

	}

}
