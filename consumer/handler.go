package consumer

import (
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

type Handler interface {
	ProcessMessage(queueInfo *opts.QueueInfo, delivery wabbitiface.WrappedDelivery)
}
