package wabbit_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/wabbit"
)

func Test_NewConnection(t *testing.T) {
	username := os.Getenv("RABBITMQ_DEFAULT_USER")
	pw := os.Getenv("RABBITMQ_DEFAULT_PASS")
	port := os.Getenv("RABBITMQ_PORT")
	host := os.Getenv("RABBITMQ_HOST")
	config := &wabbit.Config{
		Host:     host,
		Port:     port,
		UserName: username,
		Password: pw,
	}

	_, err := wabbit.NewConnection(config)
	assert.NoError(t, err)

}
