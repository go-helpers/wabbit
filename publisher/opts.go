package publisher

import (
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

type publisherOpts struct {
	bufferSize   uint64
	notifierHook wabbitiface.Notify

	channelOptions *opts.Options
	queueKeys      map[string]bool
}

func defaultPublisherOpts() *publisherOpts {
	return &publisherOpts{
		bufferSize:     defaultBufferSize,
		notifierHook:   nil,
		channelOptions: &opts.Options{},
		queueKeys:      make(map[string]bool),
	}
}

func WithMaxOutstandingMessages(size uint64) func(*publisherOpts) {
	return func(p *publisherOpts) {
		p.bufferSize = size
	}
}
func WithNotifierHook(fn wabbitiface.Notify) func(*publisherOpts) {
	return func(p *publisherOpts) {
		p.notifierHook = fn
	}
}

func WithChannelBindingOptions(chOpts ...func(*opts.Options)) func(*publisherOpts) {
	return func(p *publisherOpts) {
		for _, fn := range chOpts {
			fn(p.channelOptions)
		}

	}
}

func (opt *publisherOpts) queueOrRouteExists(keys ...string) bool {
	for _, key := range keys {
		_, ok := opt.queueKeys[key]
		if ok {
			return ok
		}
	}

	return false
}

func (opt *publisherOpts) setQueueOrRouteMarker(keys ...string) {
	for _, key := range keys {
		if key != "" {
			opt.queueKeys[key] = true
		}
	}
}
