package publisher

import (
	"errors"
	"log"
	"sync"
	"time"

	"gitlab.com/go-helpers/wabbit"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

const (
	maxWaitTime = time.Millisecond * 50
	// defaultBufferSize default number of publishings to wait for
	defaultBufferSize uint64 = 100
)

var (
	ErrNoAck           = errors.New("no ack received")
	ErrQueueListEmpty  = errors.New("no queues declared")
	ErrUndeclaredQueue = errors.New("undeclared queue")
	ErrDeliveryFailed  = errors.New("message delivery failed")
)

// PublisherObject interface received by Publisher.Send()
type PublisherObject interface {
	ContentType() string
	Content() []byte
}

/*
	Publisher publisher struct
	each publisher should only be bound to
*/
type Publisher struct {
	// conn wabbit implementation of the amqp.Connection that automatically handles reconnection
	conn wabbitiface.WrappedConnection
	ch   wabbitiface.WrappedChannel

	opts *publisherOpts

	// connUpdated channel to listen for when the connection is reset
	// when a connection has been reset, the channel needs to be reestablished
	connUpdated         chan bool
	channelDisconnected chan error

	m           *sync.Mutex
	resultMutex *sync.RWMutex
	//lastAckTag the last delivery tag received from channel Confirmation. starts at 0
	lastAckTag uint64
	// currentTag current delivery tag. starts at 1
	currentTag uint64

	hasResult []bool
}

/*
	New returns a new Publisher object,
	with the option of (a)changing buffersize, and (b) adding an error hook.
	To use the publisher, one must minimally bind one queue:

	example:

		connection, _ := wabbit.NewConnection(config)
		pubber, err := publisher.New(
			connection,
			publisher.WithNotifierHook(func(err error, in ...interface{}) {
				// bugsnag notifier etc here
				log.Println("pubber failed", err, in)
			}),
			publisher.WithChannelBindingOptions(
				opts.WithQueues(
					opts.QueueOpts(
						opts.QueueName("q3"),
						opts.QueuDurable(),
					),
					opts.QueueOpts(
						opts.QueueName("q4"),
						opts.QueuDurable(),
					),
				),
			),
		)
*/
func New(conn *wabbit.Connection, options ...func(*publisherOpts)) (*Publisher, error) {

	p := &Publisher{
		connUpdated:         make(chan bool),
		channelDisconnected: make(chan error),
		m:                   &sync.Mutex{},
		resultMutex:         &sync.RWMutex{},
		opts:                defaultPublisherOpts(),
	}
	for _, option := range options {
		option(p.opts)
	}
	if len(p.opts.channelOptions.QueueOpts) == 0 {
		return nil, ErrQueueListEmpty
	}

	conn.SetNotifierFunc(func(err error, in ...interface{}) {
		log.Printf("wabbit:publisher conn disconnected: %s", err)
		if fn := p.opts.notifierHook; fn != nil {
			fn(err)
		}
	})

	conn.SetRecoveryFunc(func(b bool) {
		p.connUpdated <- b
	})
	p.conn = conn
	if err := p.setupChannel(); err != nil {
		return nil, err
	}
	go p.connectionUpdated()

	return p, nil
}

/*
	waitForPublish waits for publish results.
	Essentially to enusre that non-acked messages are limited
*/
func (p *Publisher) waitForPublish() {
	for {
		p.resultMutex.RLock()
		count := p.currentTag - p.lastAckTag - 1
		p.resultMutex.RUnlock()

		if count < p.opts.bufferSize {
			return
		}
		time.Sleep(time.Millisecond * 50)
	}
}

// getAckTag returns the currentTag and increments it
func (p *Publisher) getAckTag() uint64 {
	p.resultMutex.RLock()
	defer p.resultMutex.RUnlock()
	output := p.currentTag
	p.currentTag++
	p.hasResult[output%p.opts.bufferSize] = true
	return output
}

func (p *Publisher) waitForAck(ackTag uint64) error {
	start := time.Now()
	for {
		p.resultMutex.RLock()
		tmp := p.hasResult[ackTag%p.opts.bufferSize]
		p.resultMutex.RUnlock()
		if tmp {
			return nil
		}
		if time.Since(start) > maxWaitTime {
			break
		}
		time.Sleep(maxWaitTime)
	}
	// ack not received within stipulated maxWaitTime
	p.resultMutex.RLock()
	p.hasResult[ackTag%p.opts.bufferSize] = true
	p.resultMutex.RUnlock()
	return ErrNoAck
}

// Send sends object to exchange/ queue
func (p *Publisher) Send(routeKeyorQueue string, obj PublisherObject) error {
	if !p.opts.queueOrRouteExists(routeKeyorQueue) {
		return ErrUndeclaredQueue
	}
	if p.ch.IsClosed() {
		p.m.Lock()
		if err := p.setupChannel(); err != nil {
			return err
		}
		p.m.Unlock()
	}
	p.m.Lock()
	p.waitForPublish()
	ackTag := p.getAckTag()

	var exchange string
	if o := p.opts.channelOptions.ExchangeBindingOpts; o != nil {
		exchange = o.Name
	}
	err := p.ch.Publish(
		exchange,
		routeKeyorQueue,
		true,
		false,
		amqp.Publishing{
			ContentType: obj.ContentType(),
			Body:        obj.Content(),
		},
	)
	p.m.Unlock()
	if err != nil {
		return err
	}

	// wait for server to acknowledge message
	return p.waitForAck(ackTag)
}

func (p *Publisher) Close() error {
	p.ch.Close()
	p.conn.Close()
	return nil

}
func (p *Publisher) setupChannel() error {
	channel, err := p.conn.Channel()
	if err != nil {
		return nil
	}
	channel.SetNotifierFunc(func(err error) {
		p.channelDisconnected <- err

	})
	if err := channel.Confirm(false); err != nil {
		return err
	}
	p.ch = channel
	log.Print("publisher wabbit:channel established")
	//declare queues
	if err := p.declareQueues(); err != nil {
		p.ch.Close()
		return err
	}
	// declare and bind queues to the exchange if it exists
	if err := p.declareExchange(); err != nil {
		p.ch.Close()
		return err
	}
	p.resetCounter()
	ack := p.ch.NotifyPublish(make(chan amqp.Confirmation, p.opts.bufferSize))
	ret := p.ch.NotifyReturn(make(chan amqp.Return, p.opts.bufferSize))
	go p.setupChannelAckHandler(ack)
	go p.setupChannelRetHandler(ret)
	return nil
}
func (p *Publisher) declareExchange() error {
	exchangeOpts := p.opts.channelOptions.ExchangeBindingOpts
	queueOpts := p.opts.channelOptions.QueueOpts
	if _, err := p.ch.DeclareExchange(exchangeOpts, p.opts.channelOptions.MustBind, queueOpts); err != nil {
		return err
	}

	return nil
}
func (p *Publisher) declareQueues() error {

	queueOpts := p.opts.channelOptions.QueueOpts
	mustVerify := p.opts.channelOptions.MustVerify

	for _, opt := range queueOpts {

		if mustVerify {
			_, err := p.ch.QueueDeclarePassive(
				opt.Name,
				opt.Durable,
				opt.AutoDelete,
				opt.Exclusive,
				opt.NoWait,
				opt.Args,
			)
			if err != nil {
				return err
			}
		}

		p.opts.setQueueOrRouteMarker(opt.Name, opt.RouteKey)

	}

	return nil
}

func (p *Publisher) resetCounter() {
	p.lastAckTag = 0
	p.currentTag = 1
	p.hasResult = make([]bool, p.opts.bufferSize, p.opts.bufferSize)

}

func (p *Publisher) setupChannelRetHandler(list <-chan amqp.Return) {
	for ret := range list {
		if fn := p.opts.notifierHook; fn != nil {
			fn(ErrDeliveryFailed, ret)
		}

	}

}
func (p *Publisher) setupChannelAckHandler(list <-chan amqp.Confirmation) {
	for confirm := range list {
		p.resultMutex.Lock()
		p.hasResult[confirm.DeliveryTag%p.opts.bufferSize] = true
		if confirm.DeliveryTag == p.lastAckTag+1 {
			for i := p.lastAckTag + 1; i < p.currentTag; i++ {
				index := i % p.opts.bufferSize
				if p.hasResult[index] {
					p.lastAckTag++
				} else {
					break
				}
			}
		}
		p.resultMutex.Unlock()

	}
}

func (p *Publisher) connectionUpdated() {
	for {
		select {
		case updated := <-p.connUpdated:
			log.Printf("wabbit:publisher connection reestablished: %v", updated)
			p.m.Lock()
			if updated {
				reconnected := p.setupChannel()
				for reconnected != nil {
					reconnected = p.setupChannel()
				}
			}
			p.m.Unlock()

		}

	}
}
