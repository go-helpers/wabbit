package wabbit

import (
	"log"
	"sync"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

var _ wabbitiface.WrappedChannel = (*Channel)(nil)

func (me *Channel) SetNotifierFunc(fn func(error)) {
	me.notifyChannelClosed = fn
}

type Channel struct {
	*amqp.Channel
	m                   sync.Mutex
	notifyChannelClosed func(err error)
}

func (me *Channel) dcListener() {
	for {
		if err := <-me.NotifyClose(make(chan *amqp.Error)); err != nil {
			me.notifyChannelClosed(err)
		}

	}

}

func newChannel(conn *amqp.Connection) (wabbitiface.WrappedChannel, error) {
	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}
	c := &Channel{
		Channel: ch,
		notifyChannelClosed: func(err error) {
			log.Printf("wabbitchan: closed: %s", err.Error())
		},
	}
	go c.dcListener()
	return c, nil
}

func (me *Channel) DeclareQueue(mustBind, mustVerify bool, queueOpts opts.QueueOptsMap) error {

	for key, opt := range queueOpts {
		if mustBind {
			queue, err := me.QueueDeclare(
				opt.Name,
				opt.Durable,
				opt.AutoDelete,
				opt.Exclusive,
				opt.NoWait,
				opt.Args,
			)
			if err != nil {
				return err
			}
			if key.Name != queue.Name {
				key.Name = queue.Name
			}
		} else {
			if mustVerify {
				_, err := me.QueueDeclarePassive(
					opt.Name,
					opt.Durable,
					opt.AutoDelete,
					opt.Exclusive,
					opt.NoWait,
					opt.Args,
				)
				if err != nil {
					return err
				}

			}
		}

	}

	return nil
}

// DeclareExchange queues should be verified before declaring exchange
// bindQueue should be true for consumers
func (me *Channel) DeclareExchange(options *opts.ExchangeBindingOpts, bindQueue bool, queueOpts opts.QueueOptsMap) (exchange string, err error) {
	if options != nil {
		switch {
		case options.MustBind:
			err = me.declareExchange(options)
		case options.MustVerify:
			err = me.declareExchangePassive(options)
		}

		if err != nil {
			return options.Name, err
		}
		if bindQueue {
			for _, queue := range queueOpts {
				err := me.QueueBind(
					queue.Name,
					queue.RouteKey,
					options.Name,
					false,
					nil,
				)
				if err != nil {
					return options.Name, err
				}
			}
		} else {
			return options.Name, nil
		}

	}
	return "", nil
}

func (me *Channel) declareExchange(options *opts.ExchangeBindingOpts) error {
	return me.ExchangeDeclare(
		options.Name,
		options.Kind,
		options.Durable,
		options.AutoDelete,
		options.Internal,
		options.NoWait,
		options.Args,
	)
}
func (me *Channel) declareExchangePassive(options *opts.ExchangeBindingOpts) error {
	return me.ExchangeDeclarePassive(
		options.Name,
		options.Kind,
		options.Durable,
		options.AutoDelete,
		options.Internal,
		options.NoWait,
		options.Args,
	)
}
