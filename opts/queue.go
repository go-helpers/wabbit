package opts

import amqp "github.com/rabbitmq/amqp091-go"

type QueueBindingOpts struct {
	Name       string
	RouteKey   string
	Durable    bool
	Quorum     bool
	AutoDelete bool
	Exclusive  bool
	NoWait     bool
	Args       amqp.Table
	ConsumerID string
}

func QueueOpts(opts ...func(*QueueBindingOpts)) *QueueBindingOpts {
	queueOpts := getDefaultQueueBindingOpts()
	for _, opt := range opts {
		opt(queueOpts)
	}
	return queueOpts
}

func QueueName(name string) func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.Name = name
	}
}
func QueueExclusive() func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.Exclusive = true
	}
}
func QueueRouteKey(routeKey string) func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.RouteKey = routeKey
	}
}
func QueuDurable() func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.Durable = true
	}
}
func QueuQuorum() func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		if opt.Args == nil {
			opt.Args = amqp.Table{}
		}

		opt.Args["x-queue-type"] = "quorum"
		opt.Quorum = true
		opt.Durable = true
	}
}

func QueuAutoDelete() func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.AutoDelete = true
	}
}
func QueuArgs(table amqp.Table) func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		if opt.Args == nil {
			opt.Args = amqp.Table{}
		}
		for k, v := range table {
			opt.Args[k] = v
		}
	}
}
func QueuNoWait() func(*QueueBindingOpts) {
	return func(opt *QueueBindingOpts) {
		opt.NoWait = true
	}
}

func getDefaultQueueBindingOpts() *QueueBindingOpts {
	return &QueueBindingOpts{}
}
