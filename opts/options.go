package opts

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

// Options channel options
type Options struct {
	ExchangeBindingOpts *ExchangeBindingOpts
	QueueOpts           QueueOptsMap
	MustBind            bool
	MustVerify          bool

	BindingArgs amqp.Table
	Concurrency int
	QOSPrefetch int
	qosGlobal   bool
	AutoAck     bool
	Exclusive   bool
	NoWait      bool
	NoLocal     bool
	Args        amqp.Table
}

type QueueOptsMap map[*QueueInfo]*QueueBindingOpts

type QueueInfo struct {
	Name       string
	ConsumerID string
	RouteKey   string
}

func WithQueues(mustBind, mustVerify bool, queueOpts ...*QueueBindingOpts) func(*Options) {
	return func(opt *Options) {
		opt.MustBind = mustBind
		opt.MustVerify = mustVerify

		if opt.QueueOpts == nil {
			opt.QueueOpts = make(map[*QueueInfo]*QueueBindingOpts)
		}

		for _, tmp := range queueOpts {

			key := &QueueInfo{
				Name:     tmp.Name,
				RouteKey: tmp.RouteKey,
			}

			if _, ok := opt.QueueOpts[key]; !ok {
				opt.QueueOpts[key] = tmp
			}
		}

	}
}

// WithExchange ...
func WithExchange(name string, bind, verify bool, settings ...func(*ExchangeBindingOpts)) func(*Options) {
	return func(o *Options) {
		if name == "" {
			return
		}
		getGefaultExchangeOpts(o).Name = name
		getGefaultExchangeOpts(o).MustBind = bind
		getGefaultExchangeOpts(o).MustVerify = verify
		if getGefaultExchangeOpts(o).MustBind {
			getGefaultExchangeOpts(o).MustVerify = false
		}

		for _, fn := range settings {
			fn(getGefaultExchangeOpts(o))

		}
	}
}

func WithConcurrency(conc int) func(*Options) {
	return func(opt *Options) {
		opt.Concurrency = conc
	}
}
func WithQOSPrefetch(prefetch int) func(*Options) {
	return func(opt *Options) {
		opt.qosGlobal = true
		opt.QOSPrefetch = prefetch
	}
}
func WithAutoAck() func(*Options) {
	return func(opt *Options) {
		opt.AutoAck = true
	}
}

func (opts *Options) UseGlobalPrefetch() bool {
	for _, v := range opts.QueueOpts {
		if value, ok := v.Args["x-queue-type"]; ok {
			if value == "quorum" {
				return false
			}
		}
	}
	return opts.qosGlobal
}
