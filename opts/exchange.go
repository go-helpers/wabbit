package opts

import amqp "github.com/rabbitmq/amqp091-go"

type ExchangeBindingOpts struct {
	Name       string
	Kind       string
	Durable    bool
	AutoDelete bool
	Internal   bool
	NoWait     bool
	Args       amqp.Table
	MustBind   bool // need to do exchange declare
	MustVerify bool // need to verify exchange exists
}

func DurableExchange() func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.Durable = true
	}
}
func WithExchangeKind(excType string) func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.Kind = excType
	}
}

func WithExchangeAutoDelete() func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.AutoDelete = true
	}
}

func WithExchangeInternal() func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.Internal = true
	}
}
func WithExchangeNoWait() func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.NoWait = true
	}
}
func WithExchangeArgs(tab amqp.Table) func(*ExchangeBindingOpts) {
	return func(op *ExchangeBindingOpts) {
		op.Args = tab
	}
}

func getGefaultExchangeOpts(options *Options) *ExchangeBindingOpts {
	if options.ExchangeBindingOpts == nil {
		options.ExchangeBindingOpts = &ExchangeBindingOpts{
			Kind: amqp.ExchangeDirect,
		}
	}
	return options.ExchangeBindingOpts

}
