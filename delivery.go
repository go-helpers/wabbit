package wabbit

import (
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

var _ wabbitiface.WrappedDelivery = (*Delivery)(nil)

func NewWrappedDelivery(d amqp.Delivery) wabbitiface.WrappedDelivery {
	return &Delivery{
		d: d,
	}
}

type Delivery struct {
	d amqp.Delivery
}

func (d Delivery) ContentType() string {
	return d.d.ContentType
}
func (d Delivery) ContentEncoding() string {
	return d.d.ContentEncoding
}
func (d Delivery) DeliveryMode() uint8 {
	return d.d.DeliveryMode
}
func (d Delivery) Priority() uint8 {
	return d.d.Priority
}
func (d Delivery) CorrelationID() string {
	return d.d.CorrelationId
}
func (d Delivery) ReplyTo() string {
	return d.d.ReplyTo
}
func (d Delivery) Expiration() string {
	return d.d.Expiration
}
func (d Delivery) MessageID() string {
	return d.d.MessageId
}
func (d Delivery) Timestamp() time.Time {
	return d.d.Timestamp
}
func (d Delivery) Type() string {
	return d.d.Type
}
func (d Delivery) UserID() string {
	return d.d.UserId
}
func (d Delivery) AppID() string {
	return d.d.AppId
}

func (d Delivery) ConsumerTag() string {
	return d.d.ConsumerTag
}
func (d Delivery) Body() []byte {
	return d.d.Body
}
func (d Delivery) MessageCount() uint32 {
	return d.d.MessageCount
}

func (d Delivery) DeliveryTag() uint64 {
	return d.d.DeliveryTag
}
func (d Delivery) Redelivered() bool {
	return d.d.Redelivered
}
func (d Delivery) Exchange() string {
	return d.d.Exchange
}
func (d Delivery) RoutingKey() string {
	return d.d.RoutingKey
}

//functions
func (d Delivery) Ack(multiple bool) error {
	return d.d.Ack(multiple)
}
func (d Delivery) Nack(multiple, requeue bool) error {
	return d.d.Nack(multiple, requeue)
}
