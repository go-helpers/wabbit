package wabbit

import (
	"log"
	"sync"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

var _ wabbitiface.WrappedConnection = (*Connection)(nil)

/*
	Connection wrapper around the amqp.Connection that handles reconnection automatically(unless disabled).
	It then calls the notofierRecovered function to notify of updates
*/
type Connection struct {
	conn *amqp.Connection
	m    sync.Mutex
	cfg  wabbitiface.ConfigIface

	notifier            []wabbitiface.Notify // notify that connection has updated
	notofierRecovered   []func(bool)
	autoReconnect       bool // only works on publishers
	useDefaultNotifiers bool
}

func DisableAutoReconnect() func(*Connection) {
	return func(me *Connection) {
		me.autoReconnect = false
	}
}

func UseDefaultNotifiers() func(*Connection) {
	return func(me *Connection) {
		me.useDefaultNotifiers = true
	}
}

// NewConnection returns a Connection pointer
func NewConnection(cfg wabbitiface.ConfigIface, opts ...func(*Connection)) (*Connection, error) {
	return newconn(cfg, opts...)
}

// Conn returns the underlying amqp.Connection pointer
func (me *Connection) Conn() *amqp.Connection {
	return me.conn
}

//SetNotifierFunc adds a notifier function that notofies disconnection
func (me *Connection) SetNotifierFunc(fn ...wabbitiface.Notify) {
	me.notifier = append(me.notifier, fn...)
}

//SetNotifierFunc adds a notifier function that notofies that the connection has been reestablished
func (me *Connection) SetRecoveryFunc(fn ...func(bool)) {
	me.notofierRecovered = append(me.notofierRecovered, fn...)
}

func newconn(cfg wabbitiface.ConfigIface, opts ...func(*Connection)) (*Connection, error) {

	connection := &Connection{
		cfg:                 cfg,
		autoReconnect:       true,
		useDefaultNotifiers: false,
	}

	for _, fn := range opts {
		fn(connection)
	}
	if connection.useDefaultNotifiers {

		connection.notifier = []wabbitiface.Notify{
			func(e error, itf ...interface{}) {
				log.Printf("wabbitconn: disconnected: %s", e.Error())
			},
		}
		connection.notofierRecovered = []func(bool){
			func(e bool) {
				log.Printf("wabbitconn: recovered: %v", e)
			},
		}

	}
	if err := connection.connect(); err != nil {
		return nil, err
	}
	go connection.dcListener()

	return connection, nil
}

// dcListener listens for disconnect and attempts to reconnect
func (me *Connection) dcListener() {
	for {
		if err := <-me.conn.NotifyClose(make(chan *amqp.Error)); err != nil {
			for _, notify := range me.notifier {
				notify(err)
			}
			if me.autoReconnect {
				me.m.Lock()
				reconnErr := me.connect()
				for reconnErr != nil {
					reconnErr = me.connect()
				}
				me.m.Unlock()
				for _, notify := range me.notofierRecovered {
					notify(true)
				}
			}

		}
	}
}

func (me *Connection) Connect() error {
	return me.connect()
}

func (me *Connection) connect() error {
	conn, err := amqp.Dial(me.cfg.URL())
	if err != nil {
		return err
	}
	me.conn = conn

	return nil
}
