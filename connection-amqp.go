package wabbit

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

func (me *Connection) Close() error {
	return me.conn.Close()
}
func (me *Connection) NotifyClose(ch chan *amqp.Error) chan *amqp.Error {
	return me.conn.NotifyClose(ch)
}

func (me *Connection) Channel() (wabbitiface.WrappedChannel, error) {

	return newChannel(me.conn)
}
