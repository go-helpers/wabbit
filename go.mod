module gitlab.com/go-helpers/wabbit

go 1.16

require (
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/rabbitmq/amqp091-go v0.0.0-20210812094702-b2a427eb7d17
	github.com/stretchr/testify v1.7.0
)
