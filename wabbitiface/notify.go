package wabbitiface

// Notify notifier func for error
type Notify func(err error, itf ...interface{})
