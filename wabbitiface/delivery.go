package wabbitiface

import "time"

type WrappedDelivery interface {
	// property getter
	ContentType() string     // MIME content type
	ContentEncoding() string // MIME content encoding
	DeliveryMode() uint8     // queue implementation use - non-persistent (1) or persistent (2)
	Priority() uint8         // queue implementation use - 0 to 9
	CorrelationID() string   // application use - correlation identifier
	ReplyTo() string         // application use - address to reply to (ex: RPC)
	Expiration() string      // implementation use - message expiration spec
	MessageID() string       // application use - message identifier
	Timestamp() time.Time    // application use - message timestamp
	Type() string            // application use - message type name
	UserID() string          // application use - creating user - should be authenticated user
	AppID() string           // application use - creating application id

	ConsumerTag() string // for consume
	Body() []byte
	MessageCount() uint32

	DeliveryTag() uint64
	Redelivered() bool
	Exchange() string   // basic.publish exchange
	RoutingKey() string // basic.publish routing key

	//functions
	Ack(multiple bool) error
	Nack(multiple, requeue bool) error
}
