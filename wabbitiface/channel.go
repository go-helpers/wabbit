package wabbitiface

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/go-helpers/wabbit/opts"
)

// WrappedChannel channel wrapped
type WrappedChannel interface {
	AMQPChannel
	SetNotifierFunc(func(error)) // notifies of channnel close error

	// DeclareQueue declares queue
	DeclareQueue(mustBind, mustVerify bool, queueOpts opts.QueueOptsMap) error
	//DeclareExchange binds exchange
	DeclareExchange(options *opts.ExchangeBindingOpts, bindQueue bool, queueOpts opts.QueueOptsMap) (string, error)
}

// AMQPChannel  amqp Channel implementation
type AMQPChannel interface {
	Consume(queue, consumer string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error)
	QueueDeclare(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) (amqp.Queue, error)
	QueueDeclarePassive(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) (amqp.Queue, error)
	Publish(exchange, key string, mandatory, immediate bool, msg amqp.Publishing) error
	Confirm(noWait bool) error
	NotifyPublish(confirm chan amqp.Confirmation) chan amqp.Confirmation
	NotifyReturn(c chan amqp.Return) chan amqp.Return
	Close() error
	IsClosed() bool
}
