package wabbitiface

import amqp "github.com/rabbitmq/amqp091-go"

// AMQPConn selective implementation of *amqp.Connection
type AMQPConn interface {
	Close() error
	Channel() (WrappedChannel, error)
	NotifyClose(chan *amqp.Error) chan *amqp.Error
}

type WrappedConnection interface {
	AMQPConn
	Conn() *amqp.Connection
	Connect() error
	SetNotifierFunc(...Notify)
	SetRecoveryFunc(...func(bool))
}
