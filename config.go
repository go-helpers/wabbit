package wabbit

import (
	"fmt"

	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

var _ wabbitiface.ConfigIface = (*Config)(nil)

type Config struct {
	Host     string
	Port     string
	UserName string
	Password string
	Vhost    string
}

func (cfg *Config) URL() string {
	dsn := "amqp://%s:%s@%s:%s/%s"
	return fmt.Sprintf(dsn, cfg.UserName, cfg.Password, cfg.Host, cfg.Port, cfg.Vhost)
}
