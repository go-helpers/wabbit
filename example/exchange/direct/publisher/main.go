package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/go-helpers/wabbit"
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/publisher"
)

func main() {
	dann()
}

type exampleHandler struct {
	pubber *publisher.Publisher
}
type mappy map[string]interface{}

func (m mappy) ContentType() string {
	return "application/json"
}
func (m mappy) Content() []byte {
	jsn, _ := json.Marshal(m)
	return jsn
}

func (e *exampleHandler) SendToQueue(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	queueName := vars["queueName"]
	bod, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	mapped := new(mappy)
	if err := json.Unmarshal(bod, &mapped); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = e.pubber.Send(queueName, mapped)
	if err != nil {
		fmt.Println("send error", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

func dann() {
	config := &wabbit.Config{
		UserName: "guest",
		Password: "guest",
		Host:     "localhost",
		Port:     "5672",
		Vhost:    "",
	}
	burrower, err := wabbit.NewConnection(config)
	if err != nil {
		log.Fatal(err)
	}
	defer burrower.Close()
	pubber, err := publisher.New(
		burrower,
		publisher.WithNotifierHook(func(err error, in ...interface{}) {
			// bugsnag notifier etc here
			log.Println("pubber failed", err, in)
		}),
		publisher.WithChannelBindingOptions(
			opts.WithExchange("tnt-direct", true, true),
			// as a publisher to an exchange, no need to care if queue or rk exists
			opts.WithQueues(
				false,
				false,
				opts.QueueOpts(
					opts.QueueName("q3"),
					opts.QueuDurable(),
					opts.QueueRouteKey("tnt1"),
				),
				opts.QueueOpts(
					opts.QueueName("q4"),
					opts.QueuDurable(),
				),
			),
		),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer pubber.Close()
	handler := &exampleHandler{
		pubber: pubber,
	}

	r := mux.NewRouter()
	r.Methods("POST").Path("/queue/{queueName:q3|q4|tnt1}").HandlerFunc(handler.SendToQueue)
	srv := &http.Server{
		Addr: ":8080",
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	srv.Shutdown(context.Background())

}
