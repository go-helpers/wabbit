package main

import (
	"log"
	"time"

	"gitlab.com/go-helpers/wabbit"
	"gitlab.com/go-helpers/wabbit/consumer"
	"gitlab.com/go-helpers/wabbit/opts"
	"gitlab.com/go-helpers/wabbit/wabbitiface"
)

func main() {
	forever := make(chan bool)
	ddn()

	<-forever

}

func ddn() {
	config := &wabbit.Config{
		UserName: "guest",
		Password: "guest",
		Host:     "localhost",
		Port:     "5672",
		Vhost:    "",
	}
	burrower, err := wabbit.NewConnection(
		config,
		wabbit.DisableAutoReconnect(),
	)
	if err != nil {
		log.Fatal(err)
	}
	cs, err := consumer.New(burrower,
		opts.WithExchange("tnt-fanout", true, false, opts.WithExchangeKind("fanout")),
		opts.WithQueues(
			true,
			false,
			opts.QueueOpts(
				opts.QueueName(""),
				opts.QueueExclusive(),
			),
			// opts.QueueOpts(
			// 	opts.QueueName("tnt"),
			// 	opts.QueuDurable(),
			// ),
		),
	)
	if err != nil {
		log.Fatal(err)

	}
	cs.Start(&handles{})
}

type handles struct{}

func (*handles) ProcessMessage(queueInfo *opts.QueueInfo, wrapped wabbitiface.WrappedDelivery) {
	defer wrapped.Ack(false)
	log.Println("queueInfo", queueInfo)
	log.Println("consumer", wrapped.ConsumerTag())
	log.Println("exchange", wrapped.Exchange())
	log.Println("body", string(wrapped.Body()))
	log.Println("routeKey", wrapped.RoutingKey())
	time.Sleep(3 * time.Second)

}
